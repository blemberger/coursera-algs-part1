import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;

public class RandomWord {
    public static void main(String[] args) {
        String candidate = "*empty*";
        int i = 0;
        while (!StdIn.isEmpty()) {
            String nextString = StdIn.readString();
            if (StdRandom.bernoulli(1.0/++i)) {
                candidate = nextString;
            }
        }
        StdOut.println(candidate);
    }
}
