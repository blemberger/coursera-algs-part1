import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {

    private static final int VIRTUAL_TOP_NODE = 0;
    private final int virtualBottomNode;
    private final boolean[][] sites;
    private final WeightedQuickUnionUF percolationUF;
    private final WeightedQuickUnionUF fullUF;
    private final int sideLength;

    private int openSites;

    /**
     * creates n-by-n grid, with all sites initially blocked
     */
    public Percolation(int n) {
        if (n <= 0) {
            throw new IllegalArgumentException("Grid must have dimensions greater than zero.");
        }
        sideLength = n;
        virtualBottomNode = n * n + 1;
        sites = new boolean[n + 1][n + 1]; // all false, meaning all blocked
        percolationUF = new WeightedQuickUnionUF(n * n + 2); // include virtual top & bottom
        fullUF = new WeightedQuickUnionUF(n * n + 1); // include only virtual top
    }

    /**
     * opens the site (row, col) if it is not open already
     */
    public void open(int row, int col) {
        if (!isOpen(row, col)) {
            sites[row][col] = true;
            openSites++;
            // union with adjacent sites if they are open & within bounds
            connectRightAdjacent(row, col);
            connectDownAdjacent(row, col);
            connectLeftAdjacent(row, col);
            connectUpAdjacent(row, col);
        }
    }

    /**
     * is the site (row, col) open?
     */
    public boolean isOpen(int row, int col) {
        verifyInBounds(row, "row");
        verifyInBounds(col, "col");
        return sites[row][col];
    }

    /**
     * is the site (row, col) full?
     */
    public boolean isFull(int row, int col) {
        verifyInBounds(row, "row");
        verifyInBounds(col, "col");
        return fullUF.find(nodeIdForRowCol(row, col)) == fullUF.find(VIRTUAL_TOP_NODE);
    }

    /**
     * returns the number of open sites
     */
    public int numberOfOpenSites() {
        return openSites;
    }

    /**
     * does the system percolate?
     */
    public boolean percolates() {
        return percolationUF.find(VIRTUAL_TOP_NODE) == percolationUF.find(virtualBottomNode);
    }

    private void verifyInBounds(int dimension, String dimensionType) {
        if (dimension < 1 || dimension > sideLength) {
            throw new IllegalArgumentException(String.format("%s must be in the range (1, %d)", dimensionType, sideLength));
        }
    }

    private int nodeIdForRowCol(int row, int col) {
        return (row - 1) * sideLength + col; // row-1 * n + col (top corner is row=1, col=1)
    }

    private void connectLeftAdjacent(int row, int col) {
        int leftCol = col - 1;
        if (leftCol > 0 && isOpen(row, leftCol)) {
            int thisNode = nodeIdForRowCol(row, col);
            int adjLeftNode = nodeIdForRowCol(row, leftCol);
            percolationUF.union(thisNode, adjLeftNode);
            fullUF.union(thisNode, adjLeftNode);
        }
    }

    private void connectRightAdjacent(int row, int col) {
        int rightCol = col + 1;
        if (rightCol <= sideLength && isOpen(row, rightCol)) {
            int thisNode = nodeIdForRowCol(row, col);
            int adjRightNode = nodeIdForRowCol(row, rightCol);
            percolationUF.union(thisNode, adjRightNode);
            fullUF.union(thisNode, adjRightNode);
        }
    }

    private void connectUpAdjacent(int row, int col) {
        int thisNode = nodeIdForRowCol(row, col);
        if (row == 1) {
            percolationUF.union(VIRTUAL_TOP_NODE, thisNode);
            fullUF.union(VIRTUAL_TOP_NODE, thisNode);
        }
        else {
            int upRow = row - 1;
            if (isOpen(upRow, col)) {
                int adjTopNode = nodeIdForRowCol(upRow, col);
                percolationUF.union(thisNode, adjTopNode);
                fullUF.union(thisNode, adjTopNode);
            }
        }
    }

    private void connectDownAdjacent(int row, int col) {
        int thisNode = nodeIdForRowCol(row, col);
        if (row == sideLength) { // at bottom row
            percolationUF.union(virtualBottomNode, thisNode); // prevent backwash: only connect using percolation data structure
        }
        else {
            int downRow = row + 1;
            if (isOpen(downRow, col)) {
                int adjDownNode = nodeIdForRowCol(downRow, col);
                percolationUF.union(thisNode, adjDownNode);
                fullUF.union(thisNode, adjDownNode);
            }
        }
    }

    /**
     * test client (optional)
     * @return
     */
    public static void main(String[] args) {
        int n = 100;
        int totalSites = n * n;
        Percolation p = new Percolation(n);
        while (!p.percolates()) {
            int tryRow = StdRandom.uniform(1, n + 1);
            int tryCol = StdRandom.uniform(1, n + 1);
            StdOut.printf("Trying row %d col %d%n", tryRow, tryCol);
            p.open(tryRow, tryCol);
            StdOut.printf("\tSites now open: %d%n", p.numberOfOpenSites());
        }
        StdOut.printf("Matrix size %d percolates after %d openings of %d sites. %% Sites open: %.2f%n", n, p.numberOfOpenSites(),
            totalSites, (double) p.numberOfOpenSites() / totalSites);
    }
}