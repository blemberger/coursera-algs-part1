import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

public class PercolationStats {

    private final int trials;
    private final double meanFractionOpenResults;
    private final double stdFractionOpenResults;

    /**
     * perform independent trials on an n-by-n grid
     * @param n
     * @param trials
     */
    public PercolationStats(int n, int trials) {
        if (n < 1 || trials < 1) {
            throw new IllegalArgumentException("Invalid parameters.");
        }
        this.trials = trials;
        double[] fractionOpenResults = new double[trials];

        // run all the trials
        for (int t = 0; t < trials; t++) {
            fractionOpenResults[t] = runTrial(n);
        }

        // set the pertinent results on fields
        meanFractionOpenResults = StdStats.mean(fractionOpenResults);
        stdFractionOpenResults = StdStats.stddev(fractionOpenResults);
    }

    /**
     * sample mean of percolation threshold
     * @return
     */
    public double mean() {
        return meanFractionOpenResults;
    }

    /**
     * sample standard deviation of percolation threshold
     * @return
     */
    public double stddev() {
        return stdFractionOpenResults;
    }

    /**
     * low endpoint of 95% confidence interval
     * @return
     */
    public double confidenceLo() {
        return meanFractionOpenResults - confidenceBound();
    }

    /**
     * high endpoint of 95% confidence interval
     * @return
     */
    public double confidenceHi() {
        return meanFractionOpenResults + confidenceBound();
    }

    /**
     * test client
     * @param args
     */
    public static void main(String[] args) {
        if (args.length == 2) {
            PercolationStats stats = new PercolationStats(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
            StdOut.printf("%-26s= %f%n", "mean", stats.mean());
            StdOut.printf("%-26s= %f%n", "stddev", stats.stddev());
            StdOut.printf("%-26s= [%f, %f]%n", "95% confidence interval", stats.confidenceLo(), stats.confidenceHi());
        }
        else {
            System.err.println("Need exactly 2 command line args: (size of grid) and (# of trials).");
        }
    }

    // Run a single trial for a (n x n) grid
    private double runTrial(int n) {
        Percolation p = new Percolation(n);
        while (!p.percolates()) {
            int tryRow = StdRandom.uniform(1, n + 1);
            int tryCol = StdRandom.uniform(1, n + 1);
            p.open(tryRow, tryCol);
        }
        return ((double) p.numberOfOpenSites()) / (n * n);
    }

    // Calculate the 95% confidence half-interval for large number of trials
    private double confidenceBound() {
        return 1.96 * stdFractionOpenResults / Math.sqrt(trials);
    }
}